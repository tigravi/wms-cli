FILE_NAME?=wms-cli
BUILD_GOOS?=linux
BUILD_GOARCH?=amd64

run: build
		main

build:
		env GODS=$(BUILD_GOOS) GOARCH=$(BUILD_GOARCH) go build -o $(HOME)/bin/$(FILE_NAME) .
		$(HOME)/bin/$(FILE_NAME) completion bash > bash_completion_$(FILE_NAME).sh
		#source ./bash_completion_$(FILE_NAME).sh
