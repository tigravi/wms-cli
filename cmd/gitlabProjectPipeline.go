/*
Copyright © 2022 tigravi@yandex.ru

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/ini.v1"
	"log"
	"os"
)

//var Repo string

// gitlabProjectPipelineCmd represents the pipelines command
var gitlabProjectPipelineCmd = &cobra.Command{
	Use:        "pipeline",
	Aliases:    nil,
	SuggestFor: nil,
	Short:      "Выводит список сборочных линий",
	Long: `Выводит список сборочных линий от новых к старым
если не указан репозиторий то он берётся из .git проекта`,
	Example: "wms-cli gitlab project pipeline git@gitlab.wmstudio.su:bitrix/test.git -l 3",
	Version: "0.1",
	Run: func(cmd *cobra.Command, args []string) {
		var gitRepo []string
		if len(args) > 0 {
			gitRepo = args
		} else {
			gitIniCfg, err := ini.Load(".git/config")
			if err != nil {
				fmt.Printf("Fail to read file: %v", err)
				os.Exit(1)
			}
			gitRepo = append(gitRepo, gitIniCfg.Section("remote \"origin\"").Key("url").String())
		}
		for _, value := range gitRepo {
			domain, path, err := RepoPars(value)
			if err != nil {
				panic(err)
			}
			token := viper.GetString("gitlab." + domain)
			if token != "" {
				git, err := gitlab.NewClient(token, gitlab.WithBaseURL("https://"+domain+"/api/v4"))
				if err != nil {
					log.Fatalf("Failed to create client: %v", err)
				}
				pipelines, qwe, err := git.Pipelines.ListProjectPipelines(path, &gitlab.ListProjectPipelinesOptions{
					ListOptions: gitlab.ListOptions{
						Page:    0,
						PerPage: LimitPage,
					},
				})
				for _, value := range pipelines {
					fmt.Println(value.ID, value.WebURL, value.Status)
				}
				fmt.Println("TotalItems:", qwe.TotalItems)
			} else {
				fmt.Println("No token:", domain)
			}
		}
		//fmt.Println("pipelines called")
	},
}

func init() {
	gitlabProjectCmd.AddCommand(gitlabProjectPipelineCmd)
	gitlabProjectPipelineCmd.Flags().IntVarP(&LimitPage, "limit", "l", 10, "ограничение вывода строк")
}
