/*
Copyright © 2022 tigravi@yandex.ru

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
)

var LimitPage int

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "wms-cli",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		viper.SetConfigName("config") // name of config file (without extension)
		//viper.SetConfigType("yml")            // REQUIRED if the config file does not have the extension in the name
		viper.AddConfigPath("/etc/wms-cli/")  // path to look for the config file in
		viper.AddConfigPath("$HOME/.wms-cli") // call multiple times to add many search paths
		//viper.AddConfigPath(".")              // optionally look for config in the working directory
		err := viper.ReadInConfig() // Find and read the config file
		if err != nil {             // Handle errors reading the config file
			panic(fmt.Errorf("fatal error config file: %w", err))
		}
		fmt.Println(viper.AllKeys())
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	viper.SetConfigName("config") // name of config file (without extension)
	//viper.SetConfigType("yml")            // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("/etc/wms-cli/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.wms-cli") // call multiple times to add many search paths
	//viper.AddConfigPath(".")              // optionally look for config in the working directory
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	//fmt.Println(viper.AllKeys())
	//rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "~/.wms-cli/config.yml", "config file (default is $HOME/.wms-cli/config.yml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	//rootCmd.Flags().IntVarP(&LimitPage, "limit", "l", 10, "ограничение вывода строк")
}
