/*
Copyright © 2022 tigravi@yandex.ru

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"net/url"
	"strings"
)

//var (
//	GitlabServer string
//	GitlabToken string
//)
// gitlabCmd represents the gitlab command
var gitlabCmd = &cobra.Command{
	Use:   "gitlab",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("gitlab called")
	},
}

func init() {
	rootCmd.AddCommand(gitlabCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// gitlabCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	//gitlabCmd.Flags().StringVarP(GitlabServer, "", "t", false, "Help message for toggle")
}

func RepoPars(repo string) (domain string, path string, err error) {
	if repo[:4] == "git@" {
		repoArr := strings.Split(repo, ":")
		domain = strings.Split(repoArr[0], "@")[1]
		path = strings.Split(repoArr[1], ".")[0]
		return domain, path, nil
	} else {
		u, err := url.Parse(repo)
		if err != nil {
			return "", "", errors.New("error RepoPars: " + repo + "(" + err.Error() + ")")
		}
		domain = u.Host
		path = (strings.Split(u.Path, ".")[0])[1:]
		return domain, path, nil
	}
	return "", "", errors.New("error RepoPars: " + repo)
}
