/*
Copyright © 2022 tigravi@yandex.ru

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/ini.v1"
	"log"
	"os"

	"github.com/spf13/cobra"
)

// gitlabProjectPipelineRmCmd represents the pipelineRm command
var pipelineList []int
var gitlabProjectPipelineRmCmd = &cobra.Command{
	Use:     "rm",
	Short:   "удалить сборочную линию",
	Long:    `удаляет сборочные линии из списка по ID`,
	Example: "wms-cli gitlab project pipeline rm git@gitlab.wmstudio.su:/bitrix/test.git -p 11264,11270",
	Version: "0.1",
	Run: func(cmd *cobra.Command, args []string) {
		var gitRepo []string
		if len(args) > 0 {
			gitRepo = args
		} else {
			gitIniCfg, err := ini.Load(".git/config")
			if err != nil {
				fmt.Printf("Fail to read file: %v", err)
				os.Exit(1)
			}
			gitRepo = append(gitRepo, gitIniCfg.Section("remote \"origin\"").Key("url").String())
		}
		for _, value := range gitRepo {
			domain, path, err := RepoPars(value)
			if err != nil {
				panic(err)
			}
			token := viper.GetString("gitlab." + domain)
			if token != "" {
				git, err := gitlab.NewClient(token, gitlab.WithBaseURL("https://"+domain+"/api/v4"))
				if err != nil {
					log.Fatalf("Failed to create client: %v", err)
				}
				for _, value := range pipelineList {
					_, err := git.Pipelines.DeletePipeline(path, value)
					if err != nil {
						fmt.Println(value, err)
					} else {
						fmt.Println(value, "remove")
					}
				}
			} else {
				fmt.Println("No token:", domain)
			}
		}
		fmt.Println("pipelineRm called")
	},
}

func init() {
	gitlabProjectPipelineCmd.AddCommand(gitlabProjectPipelineRmCmd)

	gitlabProjectPipelineRmCmd.Flags().IntSliceVarP(&pipelineList, "pipelines", "p", []int{}, "список ID сборочных линий")
}
