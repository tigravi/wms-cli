/*
Copyright © 2022 tigravi@yandex.ru

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/ini.v1"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var gitlabProjectPipelineRmAllCmd = &cobra.Command{
	Use:     "all",
	Version: "0.1",
	Short:   "удалить сборочные линии",
	Long:    `удаляет сборочные линии от старых к новым с ограничением по кол-ву за одно выполнение`,
	Example: "wms-cli gitlab project pipeline rm all git@gitlab.wmstudio.su:/bitrix/test.git -l 3",
	Run: func(cmd *cobra.Command, args []string) {
		var gitRepo []string
		if len(args) > 0 {
			gitRepo = args
		} else {
			gitIniCfg, err := ini.Load(".git/config")
			if err != nil {
				fmt.Printf("Fail to read file: %v", err)
				os.Exit(1)
			}
			gitRepo = append(gitRepo, gitIniCfg.Section("remote \"origin\"").Key("url").String())
		}
		for _, value := range gitRepo {
			domain, path, err := RepoPars(value)
			if err != nil {
				panic(err)
			}
			token := viper.GetString("gitlab." + domain)
			if token != "" {
				git, err := gitlab.NewClient(token, gitlab.WithBaseURL("https://"+domain+"/api/v4"))
				if err != nil {
					log.Fatalf("Failed to create client: %v", err)
				}
				pipelines, _, err := git.Pipelines.ListProjectPipelines(path, &gitlab.ListProjectPipelinesOptions{
					ListOptions: gitlab.ListOptions{
						Page:    0,
						PerPage: LimitPage,
					},
					Sort: gitlab.String("asc"),
				})
				for _, value := range pipelines {
					_, err := git.Pipelines.DeletePipeline(path, value.ID)
					if err != nil {
						fmt.Println(value.ID, value.WebURL, err)
					} else {
						fmt.Println(value.ID, value.WebURL, "remove")
					}
				}
			} else {
				fmt.Println("No token:", domain)
			}
		}
		fmt.Println("pipeline rm all called")
	},
}

func init() {
	gitlabProjectPipelineRmCmd.AddCommand(gitlabProjectPipelineRmAllCmd)

	gitlabProjectPipelineRmAllCmd.Flags().IntVarP(&LimitPage, "limit", "l", 10, "ограничение вывода строк")
}
